# Sublime Text plugin for Docker Compose

> A simple Sublime Text to integrate with docker compose

## Features

- Show in statusbar number of actively running conainers
- Shows red icon in status bar if not all containers are running
- Allows starting, stopping and listing runnig containers

It's an initial version that may not be polished enough yet
