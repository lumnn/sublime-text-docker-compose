import sublime

plugin_name = 'DockerCompose'

class Settings():
    settings = None

    @staticmethod
    def all():
        if not Settings.settings:
            Settings.settings = sublime.load_settings(plugin_name + '.sublime-settings')
            Settings.settings.add_on_change(plugin_name + '-reload', Settings.clear_settings)

        return Settings.settings

    @staticmethod
    def clear_settings():
        Settings.settings = None

    @staticmethod
    def get(key, default):
        settings = Settings.all()
        return settings.get(key, default)
