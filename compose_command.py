import sublime_plugin
import sublime
import subprocess
import os
from threading import Thread
from .core.settings import Settings
from .core.docker_compose import DockerCompose

class DockerComposeCmdCommand(sublime_plugin.WindowCommand):
    def run (self, subcommand):
        cmd = Settings.get('cmd', ['docker']) + ['compose'] + subcommand

        view = self.window.active_view()

        if not view:
            sublime.status_message("🐳 docker compose environment was not found")
            return

        folder = DockerCompose.get_view_compose_folder(view)

        if not folder:
            sublime.status_message("🐳 docker compose environment was not found")
            return

        panel = self.window.create_output_panel('docker_compose')
        panel.set_syntax_file("Packages/DockerCompose/DockerComposeCommandOutput.sublime-syntax")
        panel.settings().set("scroll_past_end", False)
        panel.settings().set("rulers", [])
        panel.settings().set("gutter", False)
        panel.set_read_only(False)
        self.window.run_command('show_panel', { 'panel': 'output.docker_compose' })

        env = os.environ.copy()
        env["PWD"] = folder

        process = subprocess.Popen(
            cmd,
            bufsize=1,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            env=env,
            cwd=folder,
            universal_newlines=True
        )

        thread = Thread(target = self.process_output_to_panel, kwargs={'process': process, 'panel': panel})
        thread.start()

    def process_output_to_panel(self, process, panel):
        for line in process.stdout:
            panel.run_command("append", { "characters": line })

        process.wait()

        if process.returncode != 0:
            panel.run_command("append", { "characters": "\nExit Code: {}\n".format(process.returncode) })

        panel.set_read_only(True)
