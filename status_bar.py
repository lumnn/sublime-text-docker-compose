import sublime
import sublime_plugin
import json
import subprocess
from threading import Thread
from threading import Event
from .core.settings import Settings
from .core.docker_compose import DockerCompose

log_prefix = "Docker Compose Plugin: "

class StatusBarListener(sublime_plugin.EventListener):
    def __init__(self):
        self.folder_views = {}
        self.folder_threads = {}

        print("{}Init".format(log_prefix))

    def run_schedule(self, folder, event):
        last_delay = 0
        print("{}Schedule enabled for folder '{}'".format(log_prefix, folder))

        while len(self.folder_views[folder]) > 0:
            process = subprocess.run(
                Settings.get('cmd', ['docker']) + ['compose', 'ps', '--format', 'json'],
                env={"PWD": folder},
                cwd=folder,
                capture_output=True
            )

            try:
                process.check_returncode()
            except:
                print("{}Stopped checking '{}' due to error: \n{}".format(log_prefix, folder, process.stderr.decode()))
                return

            status = json.loads(process.stdout)

            count_by_state = {}

            for container in status:
                if container['State'] not in count_by_state:
                    count_by_state[container['State']] = 1
                    continue

                count_by_state[container['State']] += 1

            running_count = count_by_state["running"] if "running" in count_by_state else 0

            if running_count != len(status):
                message = "Docker Compose: 🟥 {}/{}".format(running_count, len(status))
            else:
                message = "Docker Compose: {}".format(running_count)

            for view in self.folder_views[folder]:
                view.set_status("docker_compose", message)

            delay = Settings.get('refresh_delay', 60)

            if delay != last_delay:
                print('{}delay is now {}'.format(log_prefix, delay))
                last_delay = delay

            event.wait(delay)
            event.clear()

        print("{}Schedule disabled for folder '{}'".format(log_prefix, folder))

    def deactivate_schedule(self, view):
        folder = DockerCompose.get_view_compose_folder(view)

        if not folder:
            return

        if view in self.folder_views[folder]:
            self.folder_views[folder].remove(view)

        if len(self.folder_views[folder]) > 0:
            return

        thread = self.folder_threads.pop(folder, None)

        if not thread:
            return

        thread['event'].set()
        thread['thread'].join()

    def deactivate_window(self, window):
        for view in window.views():
            self.deactivate_schedule(view)

    def on_activated_async(self, view):
        if not view:
            return

        folder = DockerCompose.get_view_compose_folder(view)

        if not folder:
            return

        if not folder in self.folder_views:
            self.folder_views[folder] = set()

        if not view in self.folder_views:
            self.folder_views[folder].add(view)

        if folder in self.folder_threads:
            return

        event = Event()

        self.folder_threads[folder] = {
            'thread': Thread(target = self.run_schedule, kwargs = { 'folder': folder, 'event': event }),
            'event': event
        }

        self.folder_threads[folder]['thread'].start()

    def on_pre_close(self, view):
        self.deactivate_schedule(view)

    def on_pre_close_window(self, window):
        self.deactivate_window(window)

    def on_pre_close_project(self, window):
        self.deactivate_window(window)
