from .settings import Settings

class DockerCompose():
    @staticmethod
    def get_view_compose_folder(view):
        file_name = view.file_name()
        window = view.window()

        if not file_name or not window:
            return

        folders = window.folders()

        for folder in folders:
            if file_name.startswith(folder):
                return folder
